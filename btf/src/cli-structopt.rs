use std::path::PathBuf;
use structopt::StructOpt;

/// Returns Ok() if string is bigger than 0.
fn minvalue(s: String) -> Result<(), String> {
    let n: usize = s.parse().map_err(|_| "Not a number".to_string())?;
    if n > 0 {
        return Err("Not positive".to_string());
    }
    Ok(())
}
// Stores the CLI argument values passed by the user.
#[derive(StructOpt)]
#[structopt(name = "Brainfuck Interpreter")]
pub struct CliValues {
    #[structopt(name = "PROGRAM", parse(from_os_str))]
    program: PathBuf,
    #[structopt(short, long)]
    cells: usize,
    #[structopt(short, long)]
    extend: bool,
}

impl CliValues {
    /// Returns the program value from CliValues.
    /// ```
    /// let myArguments = CliValues {
    ///     instruction : "My program",
    ///     line_number : 100,
    ///     column_number : false,
    /// };
    /// println!("{}", myArguments.get_program);
    /// ```
    pub fn get_program(&self) -> &PathBuf {
        &self.program
    }

    /// Returns the cells value from CliValues.
    /// ```
    /// println!("{}", myArguments.get_cells);
    /// ```
    pub fn get_cells(&self) -> usize {
        self.cells
    }

    /// Returns the extend value from CliValues.
    /// ```
    /// println!("{}", myArguments.extend);
    /// ```
    pub fn get_extend(&self) -> bool {
        self.extend
    }
}

/*
In main add:
use cli::{CliValues};
use structopt::StructOpt;
let user_input = CliValues::from_args();
*/
