use clap::{App, Arg};
use std::path::PathBuf;

/// Stores the CLI argument values passed by the user.
pub struct CliValues {
    program: PathBuf,
    cells: usize,
    extend: bool,
}

impl CliValues {
    /// Returns the program value from CliValues.
    /// ```
    /// let myArguments = CliValues {
    ///     instruction : "My program",
    ///     line_number : 100,
    ///     column_number : false,
    /// };
    /// println!("{}", myArguments.get_program);
    /// ```
    pub fn get_program(&self) -> &PathBuf {
        &self.program
    }

    /// Returns the cells value from CliValues.
    /// ```
    /// println!("{}", myArguments.get_cells);
    /// ```
    pub fn get_cells(&self) -> usize {
        self.cells
    }

    /// Returns the extend value from CliValues.
    /// ```
    /// println!("{}", myArguments.extend);
    /// ```
    pub fn get_extend(&self) -> bool {
        self.extend
    }
}

pub fn cl_interface() -> Option<CliValues> {
    let matches = App::new("BrainFuck Interpreter")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Iker Perez del Palomar Sustatxa")
        .about("Interpretates BrainFuck programs")
        .arg(
            Arg::with_name("PROGRAM")
                .help("Program file to be interpreted")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("cells")
                .help("Specifies the numner of cells in the VM tape")
                .short("c")
                .long("cells")
                .takes_value(true)
                .require_equals(true)
                .min_values(1),
        )
        .arg(
            Arg::with_name("extend")
                .help("Makes tape's size extendible")
                .short("e")
                .long("extend")
                .takes_value(false),
        )
        .get_matches();

    Some(CliValues {
        program: PathBuf::from(matches.value_of("PROGRAM").unwrap()),
        cells: if matches.is_present("cells") {
            matches.value_of("cells").unwrap().parse().unwrap()
        } else {
            0
        },
        extend: matches.is_present("extend"),
    })
}
