use btf_interp::Vm;
use btf_types::BFprogram;
use std::io;
use std::io::Write;
use std::process::exit;

mod cli;
use cli::cl_interface;

type GenError = Box<dyn std::error::Error>;

struct Writer<T> {
    buffer: T,
    newline: bool,
}

impl<T: Write> Write for Writer<T> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if buf.last().ok_or(io::ErrorKind::NotFound)? != &b'\n' {
            self.newline = false;
        }
        self.buffer.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.buffer.flush()
    }
}

impl<T> Drop for Writer<T> {
    fn drop(&mut self) {
        if !self.newline {
            println!();
        }
    }
}

fn run_btf(user_input: &cli::CliValues) -> Result<(), GenError> {
    let mut bf_program = BFprogram::from_file(user_input.get_program())?;
    //let mut bf_program = match BFprogram::from_file(user_input.get_program()) {
    //    Ok(BFprogram) => BFprogram,
    //    Err(err) => return Err(err),
    //};
    if !bf_program.check_validity() {
        panic!("ERROR: BrainFuck program contains errors.")
    }
    let mut bf_machine: Vm<u8> =
        btf_interp::Vm::new(user_input.get_cells(), user_input.get_extend(), &bf_program);
    let stdout = Writer {
        buffer: io::stdout(),
        newline: true,
    };
    let stdin = std::io::stdin();
    bf_machine.interpret(stdin, stdout)?;
    Ok(())
}

fn main() {
    let user_input = cl_interface().unwrap();
    if let Err(e) = run_btf(&user_input) {
        // Report the error here
        println!(
            "btf: ERROR in input file {}, {}",
            user_input
                .get_program()
                .to_str()
                .unwrap_or("Couldn't convert from PathBuf to Str"),
            e
        );
        exit(1)
    }
}
