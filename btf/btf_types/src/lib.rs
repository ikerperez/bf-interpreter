use std::collections::HashMap;
use std::error;
use std::fmt::{Debug, Display, Formatter};
use std::fs;
use std::path::Path;

#[derive(Debug)]
pub enum TypeError {
    BFprogramCreationError,
    IoError(std::io::Error),
}

impl Display for TypeError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match &self {
            TypeError::BFprogramCreationError => {
                write!(f, "Error while creating the BFprogram structure")
            }
            TypeError::IoError(err) => write!(f, "IO Error: {}", err),
        }
    }
}

impl std::error::Error for TypeError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(self)
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
/// Set of possible BrainFuck instructions.
pub enum RawInstruction {
    MoveLeft,
    MoveRight,
    Increment,
    Decrement,
    Input,
    Output,
    BeginLoop,
    EndLoop,
}
impl RawInstruction {
    /// Translates input bytes to RawInstruction enum values.
    fn from_byte(instruction: u8) -> Option<RawInstruction> {
        match instruction {
            b'>' => Some(RawInstruction::MoveRight),
            b'<' => Some(RawInstruction::MoveLeft),
            b'+' => Some(RawInstruction::Increment),
            b'-' => Some(RawInstruction::Decrement),
            b',' => Some(RawInstruction::Input),
            b'.' => Some(RawInstruction::Output),
            b'[' => Some(RawInstruction::BeginLoop),
            b']' => Some(RawInstruction::EndLoop),
            _ => None,
        }
    }
}
#[derive(Debug, Clone, PartialEq, Copy)]
/// Structure to save each BF instruction's RawInstruction value, line number and column number.
pub struct Instruction {
    instruction: RawInstruction,
    line_number: usize,
    column_number: usize,
}

impl Instruction {
    /// Returns the RawInstruction value from the specified Instruction.
    /// ```no run
    /// use btf_types::{Instruction, RawInstruction};
    /// let bf_instruction = Instruction {
    ///     instruction : RawInstruction::MoveLeft,
    ///     line_number : 1,
    ///     column_number : 1,
    /// };
    /// println!("{:?}", bf_instruction.get_instruction());
    /// println!("{}", bf_instruction.get_line_number());
    /// println!("{}", bf_instruction.get_column_number());
    /// ```
    pub fn get_instruction(&self) -> RawInstruction {
        self.instruction
    }
    /// Returns the line_number value from the specified Instruction.
    /// Example in get_instruction doc
    pub fn get_line_number(&self) -> usize {
        self.line_number
    }
    /// Returns the column_number value from the specified Instruction.
    /// Example in get_instruction doc
    pub fn get_column_number(&self) -> usize {
        self.column_number
    }
}

#[derive(Debug)]
///Structure to represent the BF program in use.
pub struct BFprogram {
    filename: String,
    instructions: Vec<Instruction>,
    pub brackets_matching: HashMap<usize, usize>,
}

impl BFprogram {
    ///Creates a BFprogram representation of the desired BF program
    /// ```no run
    /// let input_path = ("path/to/my/file");
    /// let content: String = std::fs::read_to_string(input_path).unwrap();
    /// let bf_program = btf_types::BFprogram::new(input_path, content);
    /// ```
    pub fn new<P: AsRef<Path>>(input_file: P, content: String) -> Option<BFprogram> {
        //Create a vector of instruction structures to store the instructions
        let mut instructions_set: Vec<Instruction> = Vec::new();

        /* This code is my previous solution to the problem, I left it there as a
         reminder that Rust tools to obtain neater solutions.
         Create a byte iterator of the content.
         let instructions = content.bytes();
         let mut count1: usize = 1;
         let mut count2: usize = 1;
         //Loop over each instruction
         for instruction in instructions {
             if let Some(command) = RawInstruction::from_byte(instruction) {
                 let current_instruction = Instruction {
                     instruction: command,
                     line_number: count1,
                     column_number: count2,
                 };
                 //Save current instruction structure in the instruction_set vector.
                 instructions_set.push(current_instruction);
                 count2 += 1;
             } else if instruction == b'\n' {
                 count1 += 1;
                 count2 = 1;
             } else {
                 count2 += 1;
             }
         }
        */

        //Loop over each line of the content while counting them.
        for (line_num, line) in content.lines().enumerate() {
            //Loop over each column inside a line of the content while counting them.
            for (column_num, unit) in line.bytes().enumerate() {
                /*
                If the current byte is a valid BF command store it in a Instruction
                structure with the line and column numbers.
                */
                if let Some(command) = RawInstruction::from_byte(unit) {
                    let current_instruction = Instruction {
                        instruction: command,
                        line_number: line_num + 1,
                        column_number: column_num + 1,
                    };
                    // Push the stored Instruction to the vector of instructions.
                    instructions_set.push(current_instruction);
                }
            }
        }
        // Create the BFprogram structure representing the BF program
        let program = BFprogram {
            filename: input_file
                // Convert input_file to a valid String value.
                .as_ref()
                .file_name()
                .expect("ERROR: Couldn't extract filename from path")
                .to_str()
                .expect("ERROR: Couldn't convert OsString to str")
                .to_string(),
            instructions: instructions_set,
            brackets_matching: HashMap::new(),
        };
        Some(program)
    }

    /// Returns BFprogram structure representing BF program from a file path.
    pub fn from_file<P: AsRef<Path>>(input_file: P) -> Result<Self, TypeError> {
        let input_path = input_file.as_ref();
        let content: String =
            fs::read_to_string(input_path).map_err(|err| TypeError::IoError(err))?;
        BFprogram::new(input_path, content).ok_or(TypeError::BFprogramCreationError)
    }

    /// Returns the filename stored in a BFprogram
    /// ```no run
    /// let bf_program = btf_types::BFprogram::from_file("path/to/the/file").unwrap();
    /// let filename = bf_program.read_filename();
    /// ```
    pub fn read_filename(&self) -> Option<&String> {
        Some(&self.filename)
    }
    /// Returns the vector of Instructions stored in a BFprogram
    /// ```no run
    /// let bf_program = btf_types::BFprogram::from_file("path/to/the/file").unwrap();
    /// let filename = bf_program.read_instructions();
    /// ```
    pub fn read_instructions(&self) -> Option<&[Instruction]> {
        Some(&self.instructions)
    }

    /// Checks whether the loaded BF program is valid and stores the positions of the
    /// matching brackets.
    /// ```no run
    /// let bf_program = btf_types::BFprogram::from_file("path/to/the/file").unwrap();
    ///     if !bf_program.check_validity() {
    ///         panic!("ERROR: BrainFuck program contains errors.")
    ///     }
    /// ```
    pub fn check_validity(&mut self) -> bool {
        let mut brackets = Vec::new();
        //Stores the program counter value of each bracket and it's matching one
        let mut bracket_pc = Vec::new();
        //Current pc value
        let mut position: usize = 0;
        //Previous pc value
        let mut inv_position: usize;

        for instruction in &self.instructions {
            if instruction.get_instruction() == RawInstruction::BeginLoop {
                //Store in a vector the current loop bracket
                brackets.push(instruction);
                //Store in a vector the current loop bracket position
                bracket_pc.push(position);
            } else if instruction.get_instruction() == RawInstruction::EndLoop {
                if brackets.is_empty() {
                    println!(
                        "ERROR: Found an unecessary closing loop symbol in line {}
                        column {}",
                        instruction.get_line_number(),
                        instruction.get_column_number()
                    );
                    return false;
                } else {
                    brackets.pop();
                    inv_position = bracket_pc.pop().unwrap();
                    //Store the position of the begin loop bracket with the
                    //corresponding finish loop bracket position.
                    self.brackets_matching.insert(inv_position, position);
                    //Store the position of the finish loop bracket with the
                    //corresponding begin loop bracket position.
                    self.brackets_matching.insert(position, inv_position);
                }
            }
            position += 1;
        }
        if !brackets.is_empty() {
            println!("ERROR: Missing {} clossing loop symbols", brackets.len());
            for bracket in brackets {
                println!(
                    "Not matching close bracket found for bracket at line {} column {}",
                    bracket.get_line_number(),
                    bracket.get_column_number()
                );
            }
            false
        } else {
            true
        }
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    #[test]
    /// Tests correct parsing of byte BF commands to RawInstruction values.
    fn parse_instructions() {
        assert_eq!(
            RawInstruction::from_byte(b'>').unwrap(),
            RawInstruction::MoveRight
        );
        assert_eq!(
            RawInstruction::from_byte(b'<').unwrap(),
            RawInstruction::MoveLeft
        );
        assert_eq!(
            RawInstruction::from_byte(b'+').unwrap(),
            RawInstruction::Increment
        );
        assert_eq!(
            RawInstruction::from_byte(b'-').unwrap(),
            RawInstruction::Decrement
        );
        assert_eq!(
            RawInstruction::from_byte(b',').unwrap(),
            RawInstruction::Input
        );
        assert_eq!(
            RawInstruction::from_byte(b'.').unwrap(),
            RawInstruction::Output
        );
        assert_eq!(
            RawInstruction::from_byte(b'[').unwrap(),
            RawInstruction::BeginLoop
        );
        assert_eq!(
            RawInstruction::from_byte(b']').unwrap(),
            RawInstruction::EndLoop
        );
        assert_eq!(RawInstruction::from_byte(b'*'), None);
    }

    #[test]
    /// Tests correct formation of the BFprogram representation.
    fn track_line_colum() {
        let program = String::from("tototototo><\n.,*&]{\n-+");
        let line = [1, 1, 2, 2, 2, 3, 3];
        let column = [11, 12, 1, 2, 5, 1, 2];
        let bf = BFprogram::new("input_file", program).unwrap();
        for element in 0..bf.instructions.len() {
            assert_eq!(line[element], bf.instructions[element].line_number,
                "Line Error: line[element]={}, bf.instructions[element].line_number={}, instruction={:?}",
                 line[element],bf.instructions[element].line_number, bf.instructions[element].instruction);
            assert_eq!(column[element], bf.instructions[element].column_number,
                "Colum Error: column[element]={}, bf.instructions[element].column_number={}, instruction={:?}",
                 column[element],bf.instructions[element].column_number, bf.instructions[element].instruction);
        }
    }

    #[test]
    /// Tests correct functioning of the BF program validator
    fn check_validator() {
        let program_1 = String::from("tototototo><\n.,*&]{\n-+");
        let program_2 = String::from("tototototo><\n.,*&[{\n-+");
        let program_3 = String::from("tototototo><\n.,*&[]{\n-+");

        let mut bf1 = BFprogram::new("input_file", program_1).unwrap();
        let mut bf2 = BFprogram::new("input_file", program_2).unwrap();
        let mut bf3 = BFprogram::new("input_file", program_3).unwrap();

        assert_eq!(bf1.check_validity(), false, "btf1 failed");
        assert_eq!(bf2.check_validity(), false, "btf2 failed");
        assert_eq!(bf3.check_validity(), true, "btf3 failed");
    }
}
