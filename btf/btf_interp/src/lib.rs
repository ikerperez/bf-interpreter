use btf_types::{BFprogram, RawInstruction};
use std::error;
use std::fmt::{Debug, Display, Formatter};
use std::io::{Error, Read, Write};

#[derive(Debug)]
pub enum VmError {
    HeadInvalidPosition(RawInstruction),
    MissingInstructions(usize),
    IOError(Error, RawInstruction),
    MissingData,
    PcError,
}

impl Display for VmError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match &self {
            VmError::HeadInvalidPosition(err) => write!(f, "Invalid Head position {:?}", err),
            VmError::MissingInstructions(pc) => write!(f, "Missing Instruction, pc={}", pc),
            VmError::IOError(err, instruction) => {
                write!(f, "IO error {}, with {:?} instruction", err, instruction)
            }
            VmError::MissingData => write!(f, "Missing data, the cell is empty"),
            VmError::PcError => write!(f, "Not PC valid value"),
        }
    }
}

impl std::error::Error for VmError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(self)
    }
}

//A trait defines a behaviour. When applied to a generic type, this makes sire
//that any data type that can be placed in that generic type neds to satisfy
//the behaviour of that trait, that is, implement all the methods that
//the trait has defined within.
///The CellKin trait defines the required behaviour why the data types
///intending to get stored in Vm::tape. Make sure those generic types
/// implement the traits defined after "Cellkind:"
pub trait CellKind: Debug + Clone + Default {
    ///Makes sure there is a method to increment the cell value.
    fn wrapping_increment(&mut self);
    ///Makes sure there is a method to decrement the cell value.
    fn wrapping_decrement(&mut self);
    ///Implement method to read into cell, requiere u8
    fn set(&mut self, value: u8);
    ///Implement method to write from, requiere u8
    fn get(&self) -> u8;
}

impl CellKind for u8 {
    ///Implement wrapping_add method
    fn wrapping_increment(&mut self) {
        *self = self.wrapping_add(1);
    }
    ///Implement wrapping_sub method
    fn wrapping_decrement(&mut self) {
        *self = self.wrapping_sub(1);
    }
    ///Implement set
    fn set(&mut self, value: u8) {
        *self = value
    }
    ///Implement get
    fn get(&self) -> u8 {
        *self
    }
}

#[derive(Clone)]
///Virtual machine to represent the BF interpreter
pub struct Vm<'a, T: CellKind> {
    ///An array of data cells in the Brainfuck Virtual Machine.
    tape: Vec<T>,
    ///The data pointer. This is analogous to the head reading a data tape. Pointer to somewhere in tape
    head: usize,
    //Tells if the tape can or cannot grow.
    grow: bool,
    ///The program Counter. This is the index of the current instruction.
    pc: usize,
    ///Reference to the program the Vm us woking with
    program: &'a BFprogram,
}

impl<'a, T: CellKind> Vm<'a, T> {
    /// Create a Vm structure.
    /// amount: defines tape vector size, default value 30000 will be used if
    /// 0 is specified.
    /// growable: allows to make the tape size growable.
    pub fn new(amount: usize, growable: bool, bfprogram: &'a BFprogram) -> Vm<T> {
        let size: usize = if amount != 0 { amount } else { 30000 };

        Vm {
            pc: 0,
            head: 0,
            grow: growable,
            tape: vec![Default::default(); size],
            program: bfprogram,
        }
    }

    ///Increase by one the head location
    pub fn move_head_left(&mut self) -> Result<usize, VmError> {
        if self.head < 1 {
            return Err(VmError::HeadInvalidPosition(self.current_instruction()?));
        }
        self.head -= 1;
        Ok(self.pc + 1)
    }

    ///Decrease by one the head location
    pub fn move_head_right(&mut self) -> Result<usize, VmError> {
        if self.head + 1 > self.tape.len() && !self.grow {
            return Err(VmError::HeadInvalidPosition(self.current_instruction()?));
        }

        self.head += 1;
        Ok(self.pc + 1)
    }

    pub fn print_bf_struct(&self, bf_struct: &BFprogram) {
        let bf_instructions = bf_struct.read_instructions().unwrap();
        let bf_file = bf_struct.read_filename().unwrap();
        for bf_instruction in bf_instructions {
            println!(
                "[{}:{}:{}] {:?}",
                bf_file,
                bf_instruction.get_line_number(),
                bf_instruction.get_column_number(),
                bf_instruction.get_instruction()
            );
        }
    }

    ///Retrieves the current instruction in use.
    fn current_instruction(&self) -> Result<RawInstruction, VmError> {
        self.program
            .read_instructions()
            .ok_or(VmError::MissingInstructions(self.pc))
            .map(|instructions| instructions[self.head].get_instruction())
        // match self.program.read_instructions() {
        //     Some(instructions) => Ok(instructions[self.head].get_instruction()),
        //     None => Err(VmError::MissingInstructions),
        // }
    }

    /// The VM reads external data and stores it in a cell
    /// # Example
    ///
    /// ```
    /// # use bft_interp::BrainfuckVM;
    /// # use bft_types::BrainfuckProg;
    /// # use std::io;
    /// let prog = BrainfuckProg::new("fake/path.bf", "<>[[[]-]+],.");
    /// let mut bfvm: BrainfuckVM<u8> = BrainfuckVM::new(&prog, 0, false);
    /// let mut reader = io::Cursor::new(vec![42]);
    /// assert!(bfvm.cell_read(&mut reader).is_ok());
    /// ```
    fn read_to_cell<U: Read>(&mut self, reader: &mut U) -> Result<usize, VmError> {
        let mut buffer = [0];
        reader
            .read(&mut buffer[..])
            .map_err(|x| VmError::IOError(x, self.current_instruction().unwrap()))?;
        self.tape[self.head].set(buffer[0]);
        Ok(self.pc + 1)
    }

    /// The VM reads internal data from a cell and writes it to an external writer
    ///
    /// # Example
    ///
    /// ```
    /// # use bft_interp::BrainfuckVM;
    /// # use bft_types::BrainfuckProg;
    /// # use std::io;
    /// let prog = BrainfuckProg::new("fake/path.bf", "<>[[[]-]+],.");
    /// let mut bfvm: BrainfuckVM<u8> = BrainfuckVM::new(&prog, 0, false);
    /// let mut buff = io::Cursor::new(vec![42]);
    /// assert!(bfvm.cell_write(&mut buff).is_ok());
    /// assert_eq!(buff.into_inner()[0], 0);
    /// ```
    fn write_from_cell(&self, writer: &mut impl Write) -> Result<usize, VmError> {
        let buffer = [self.tape[self.head].get()];
        writer
            .write(&buffer)
            .map_err(|x| VmError::IOError(x, self.current_instruction().unwrap()))?;
        writer
            .flush()
            .map_err(|x| VmError::IOError(x, self.current_instruction().unwrap()))?;
        Ok(self.pc + 1)
    }

    /// Increment the value in a cell
    /// See test_increment_decrement for examples
    fn cell_increment(&mut self) -> Result<usize, VmError> {
        self.tape[self.head].wrapping_increment();
        Ok(self.pc + 1)
    }

    /// Decrement the value in a cell
    /// See test_increment_decrement for examples
    fn cell_decrement(&mut self) -> Result<usize, VmError> {
        self.tape[self.head].wrapping_decrement();
        Ok(self.pc + 1)
    }

    ///For a "[" bracket, move PC to the next instruction after
    /// the corresponding "]" bracket.
    fn begin_loop(&mut self) -> Result<usize, VmError> {
        if self.tape[self.head].get() == 0 {
            self.pc = self
                .program
                .brackets_matching
                .get(&self.pc)
                .ok_or(VmError::PcError)?
                + 1;
        } else {
            self.pc += 1;
        }
        Ok(self.pc)
    }

    /// For a "]" bracket if the cell value is not cero, move pc to the next instruction
    /// after the corresponding "[". Otherwise, carry on forward.
    fn end_loop(&mut self) -> Result<usize, VmError> {
        if self.tape[self.head].get() != 0 {
            self.pc = self
                .program
                .brackets_matching
                .get(&self.pc)
                .ok_or_else(|| VmError::PcError)?
                + 1;
        } else {
            self.pc += 1;
        }
        Ok(self.pc)
    }

    ///Interpretes the input program, caling various functions depending of the command to be executed
    pub fn interpret<R, W>(&mut self, mut input: R, mut output: W) -> Result<(), VmError>
    where
        R: Read,
        W: Write,
    {
        let instructions = self
            .program
            .read_instructions()
            .ok_or(VmError::MissingInstructions(self.pc))?;
        while self.pc < instructions.len() {
            self.pc = match instructions[self.pc].get_instruction() {
                RawInstruction::BeginLoop => self.begin_loop(),
                RawInstruction::EndLoop => self.end_loop(),
                RawInstruction::Decrement => self.cell_decrement(),
                RawInstruction::Increment => self.cell_increment(),
                RawInstruction::Input => self.read_to_cell(&mut input),
                RawInstruction::Output => self.write_from_cell(&mut output),
                RawInstruction::MoveLeft => self.move_head_left(),
                RawInstruction::MoveRight => self.move_head_right(),
            }?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use btf_types::RawInstruction;
    use std::io::Cursor;

    #[test]
    fn test_current_instruction() {
        let program = String::from("><.,*&]{");
        let bf = btf_types::BFprogram::new("testfile.txt", program).unwrap();
        let mut vm_test: Vm<u8> = Vm::new(8, false, &bf);
        vm_test.move_head_left();
        assert_eq!(
            vm_test.current_instruction().unwrap(),
            RawInstruction::MoveRight
        );
    }

    #[test]
    fn test_move_head() {
        let program = String::from("><");
        let bf = btf_types::BFprogram::new("testfile.txt", program).unwrap();
        let mut vm_test: Vm<u8> = Vm::new(2, false, &bf);
        println!("tape len ={}", vm_test.tape.len());
        assert!(vm_test.move_head_left().is_err());
        vm_test.move_head_right();
        assert!(vm_test.current_instruction().is_ok());
        vm_test.move_head_right();
        assert!(vm_test.move_head_right().is_err()); //panics here instead of returning error, gotta check that.
    }

    #[test]
    fn test_wrapping() {
        let program = String::from("+-");
        let bf = btf_types::BFprogram::new("testfile.txt", program).unwrap();
        let mut vm_test: Vm<u8> = Vm::new(4, false, &bf);
        assert_eq!(vm_test.tape[0], 0);
        vm_test.tape[0].wrapping_increment();
        assert_eq!(vm_test.tape[0], 1);
        vm_test.tape[0].wrapping_decrement();
        assert_eq!(vm_test.tape[0], 0);
    }

    #[test]
    fn test_increment_decrement() {
        let program = String::from("+-");
        let bf = btf_types::BFprogram::new("testfile.txt", program).unwrap();
        let mut vm_test: Vm<u8> = Vm::new(2, false, &bf);
        assert_eq!(vm_test.tape[0], 0);
        vm_test.cell_increment();
        assert_eq!(vm_test.tape[0], 1);
        vm_test.cell_decrement();
        assert_eq!(vm_test.tape[0], 0);
    }

    #[test]
    fn test_io() {
        let program = String::from("><.,*&]{");
        let bf = btf_types::BFprogram::new("testfile.txt", program).unwrap();
        let mut vm_test: Vm<u8> = Vm::new(8, false, &bf);
        use std::io::Cursor;
        let mut data = Cursor::new(vec![1]);
        vm_test.read_to_cell(&mut data);
        assert_eq!(vm_test.tape[vm_test.head].get(), 1);
        data.get_mut().clear();
        vm_test.write_from_cell(&mut data);
        assert_eq!(data.into_inner(), vec![0, 1]);
    }

    #[test]
    fn test_begin_end_loop() {
        let program = String::from("[+++++]---");
        let mut bf = btf_types::BFprogram::new("testfile.txt", program).unwrap();
        bf.check_validity();
        println!("bf1 = {:?}", bf.brackets_matching);
        let mut vm_test: Vm<u8> = Vm::new(8, false, &bf);
        assert_eq!(vm_test.begin_loop().unwrap(), 7);
        vm_test.tape[vm_test.head] = 1;
        vm_test.pc = 6;
        assert_eq!(vm_test.end_loop().unwrap(), 1);
    }

    #[test]
    fn test_interp() {
        let program = String::from("++>+>><+++++-->+++++++[-]");
        let mut bf = btf_types::BFprogram::new("testfile.txt", program).unwrap();
        bf.check_validity();
        let mut vm_test: Vm<u8> = Vm::new(8, false, &bf);
        let reader = Cursor::new(Vec::new());
        let writer = Cursor::new(Vec::new());
        vm_test.interpret(reader, writer);
        println!("tape = {:?}", vm_test.tape);
        assert_eq!(vm_test.tape[0].get(), 2);
        assert_eq!(vm_test.tape[1].get(), 1);
        assert_eq!(vm_test.tape[2].get(), 3);
        assert_eq!(vm_test.tape[3].get(), 0);
    }
}
